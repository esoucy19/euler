import unittest

from euler2 import fib


class TestEuler2(unittest.TestCase):
    def test_fib(self):
        self.assertEqual(fib(10), 89)


if __name__ == '__main__':
    unittest.main()
