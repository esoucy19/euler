"""Euler problem 2.

Each new term in the Fibonacci sequence is generated by adding the previous two
terms. By starting with 1 and 2, the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence whose values do not exceed
four million, find the sum of the even-valued terms.
"""
import functools


def euler2():
    """Entry function."""
    return sum(x for x in fib_up_to(4000000) if x % 2 == 0)


@functools.lru_cache()
def fib(n):
    """Return the nth fib number, starting at 1, 2."""
    if n == 0:
        return 1
    if n == 1:
        return 1
    if n == 2:
        return 2
    else:
        return fib(n - 1) + fib(n - 2)


def fib_gen():
    """Return a generator of fib numbers."""
    a, b = 1, 2
    while True:
        yield a
        a, b = b, a + b


def fib_up_to(n):
    """Return a generator of fib numbers under 4 million."""
    for f in fib_gen():
        if f < n:
            yield f
        else:
            raise StopIteration
