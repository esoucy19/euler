import unittest

from euler1_2 import euler1


class TestEuler1_2(unittest.TestCase):
    def test_basic_case(self):
        self.assertEqual(euler1((3, 5), 10), 23)

    def test_three_nums(self):
        """(2, 4, 6, 8), (3, 6, 9), (5)
        2 + 4 + 6 + 8 + 3 + 9 + 5 = 37
        """
        self.assertEqual(euler1((2, 3, 5), 10), 37)

    def test_problem_solution(self):
        self.assertEqual(euler1((3, 5), 1000), 233168)


if __name__ == '__main__':
    unittest.main()
