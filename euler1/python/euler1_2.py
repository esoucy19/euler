"""Euler1.

If we list all the natural numbers below 10 that are multiples of 3 or 5, we
get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

Proposed method:
- For each number, get the highest multiplier by dividing ceiling by number
and rounding down.
- Generate a list of all multipliers for each number
- Take the list of all the multipliers for all numbers, and convert it so
a set to eliminate all duplicates.
- Return the sum of the list of multipliers.
"""
from itertools import chain
from operator import mul


def euler1(nums, ceiling):
    """Entry function.

    nums: tuple(int)
    ceiling: int
    return: int
    """
    return sum(set(flatten(get_divisables(num, ceiling) for num in nums)))


def get_divisables(num, ceiling):
    """Get all numbers divisable by num below ceiling."""
    return tuple(mul(num, i) for i in range(1, int(((ceiling - 1) / num) + 1)))


def flatten(iter):
    """Flatten an iterable of iterables."""
    return chain.from_iterable(iter)
